<?php
 /**
  * @Author Darren Hamilton
  * @Date 2017
  * @License MIT
  *
  * Very Simple Version System
  *
  */
include_once('./config.php');

     /**
      * Class VSVSObject
      *
      * This is the Base Object which other Versioned Objects should inherit 
      *
      */
    class VSVSObject {

         // Static Properties
        public static $database_handle = null;

         // ==========
         // The VSVSObject makes the following properties available through it's lifecycle
         // ==========
         // guid - Assigned when an object is created
         // version_guid - Assigned immediately before an object is saved
         // created - Assigned by the database when it is inserted, only available on objects retrieved from the database
         // ==========
         // Static Functions
         // ==========
         // guid - Creates a new GUID, 
         // db - Returns a PDO Connection - use this to execute prepared statements, see save(), getByGUID(), isLatest(), getHistoryByGUID(), and getAll() for example usages
         // getByGUID($guid, $version_guid[optional]) - find the object matching the supplied guid and version_guid
         // isLatest($guid, $version_guid) - returns true if version_guid is the latest instance of the object guid in the database, or false otherwise
         // getHistoryByGUID($guid) - returns an array of every instance of a given object in the history
         // getAll() - get all the most recent version of every object in the database
         // ==========
         // Object Functions
         // ==========
         // save() - Saves an object
         //
        public $data;

         // Object Properties
        public $table;

         /**
          * Create a new VSVSObject by supplying an array containing the object's data and defining the database table it should save to
          * Overridding the table variable in function signitures of children of VSVSObject will save defining the table each time that object is created
          * As the function default constructor below demonstraights in defining `test_object` - the table used in the default examples
          */
        public function __construct($data = Array(), $table = 'test_object') {

             // If there is no GUID, assign one now
            if(!isset($data['guid'])) {
                $data['guid'] = VSVSObject::guid();
                $data['state_guid'] = VSVS_CONST_CREATED; // Just created
            }

            // Remember the data and the table
            $this->data = $data;
            $this->table = $table;
        }

         /**
          * Static Function getMacAddress
          *
          * @returns String $mac_address - returns the server's mac address without a byte separator for use in the guid
          */
        public static function getMacAddress() {

            $mac_address = '';

            // Detect OS
            if ('LINUX' == strtoupper(substr(PHP_OS, 0, 5))) {
                $console_output = `ifconfig`; // Linux command to get mac address

                $mac_address_position = strpos($console_output, 'HWaddr'); // Find the position of Physical text
                $mac_address = substr($console_output, ($mac_address_position + 7), 17); // Get Physical Address
                $mac_address = str_replace (':', '', $mac_address); // On Linux the Mac Address is : Separated
            } else if ('WIN' === strtoupper(substr(PHP_OS, 0, 3))) {
                $console_output = `ipconfig /all`;  // Windows command to get mac addres=

                $mac_address_position = strpos($console_output, 'Physical'); // Find the position of Physical text
                $mac_address = substr($console_output, ($mac_address_position + 36), 17); // Get Physical Address
                $mac_address = str_replace ('-', '', $mac_address); // On Windows the Mac Address is - Separated
            } else {
                throw new Exception('VSVSObject::getMacAddress() - UNSUPPORTED OS!');
            }

            if(strlen($mac_address) !== 12) {
                throw new Exception('VSVSObject::getMacAddress() - MAC ADDRESS NOT DETECTED');
            }

            return $mac_address;
        }

         /**
          * Function guid
          * 
          * Creates a 20 byte GUID that should be:
          * 
          * - Unique across devices
          * - Contains the mac address of the device that created it
          * - Will create increasing values with time so will index naturally in the database
          * - HEX encoded represented as a String(this will need converted to binary with hex2bin() before inserting into databases)
          *
          * @returns String $guid - a 20 byte guid in the format <version> <seconds> <miliseconds> <mac_address> <random bytes>
          */
        public static function guid() {
            $version = '1';

             // Separate the time on the space
            $time =  explode(' ', microtime());

             // Remove  the leading zero and the decimal from the micro_seconds
            $micro_seconds = (float)substr($time[0], 2);

              // Encode the $micro_seconds as 7 padded hex digits
            $micro_seconds = '00000' . dechex($micro_seconds);
            $micro_seconds =  substr($micro_seconds, (strlen($micro_seconds) - 7));

              // Encode the seconds in hex
            $seconds = dechex((float)$time[1]);

             // Get the Mac Address
            $mac_address = VSVSObject::getMacAddress();

             // Get 6 random bytes
            $random = bin2hex(random_bytes(6));

             // Combine and convert to upper case
            $guid =  strtoupper($version . $seconds . $micro_seconds . $mac_address . $random);

            return $guid;
        }

         /**
          * Magic __get method
          * 
          * If an attribute of that name exists in the data for this object return it, otherwise return null
          */
        public function __get(String $name) {

            if(array_key_exists($name, $this->data)) {
                return $this->data[$name];
            }

            return null;
        }

         /**
          * Magic __set method
          *
          * Add an attribute of the given name and value to this object
          */
        public function __set(String $name, $value) {
            $this->data[$name] = $value;
        }

         /******
          * Function save
          *
          * Saves the current object to the database
          *
          * In classes that extend VSVSObject and override the table correctly this method may not need to be overridden.
          * One case where you might need to make changes to the save function of a child object would be if that child had GUID foreign keys.
          *
          * *** GUID's must be hex2bin encoded!!!!!! ***
          *
          * As long as all the values stored in the object's $data Array have keys that match database table columns the VSVSObject::save function
          * will automatically save all values contained within the $data Array.
          *  
          * It is possible to create a child class to prepare the data, use the parent::save() function, and restore the original values after the object is saved.
          *  
          * Numerical/String values in the $data Array of an object should have array keys that correspond exactly to the names of columns in the table it saves to
          *  
          * Eg: $object->name; // Corresponds to `name` column in table
          *  
          * // Example of a class extending VSVSObject's save function to encode an editional GUID field then restore the field to its HEX value afterwards:
          * // As long as the database table contains a column called `variable_name_containing_guid` the value will be saved automatically when parent::save(); is called -
          * // variables containing data that can be inserted as is need not be converted.
          * public function save() {
          *     $backup_variable_name_containing_guid = $variable_name_containing_guid; // Make a copy of the original GUID we can restore to after saving
          *     $this->variable_name_containing_guid = hex2bin($this->variable_name_containing_guid); // Convert the GUID to Binary
          *     parent::save(); // Save the object
          *     $this_variable_name_containing_guid = $backup_variable_name_containing_guid; // Restore the HEX encoded version of the binary key
          * } 
          * 
          * That's it! No further object management code required.
          * 
          */
        public function save() {
            $table = $this->table;
            $data = $this->data;

            $fields = '';
            $markedUpFields = '';

            $this->version_guid = VSVSObject::guid(); // Get a new version, and it to this object
            $data['version_guid'] = hex2bin($this->version_guid);
            $data['guid'] = hex2bin($this->guid);

                 // If the data includes a creation date, remove it - this should only be assigned by the database
                if(isset($data['created'])) {
                    unset($data['created']);

                    // If it has a creation date and the state is created, we know this is an edit(because it must have already have been created)!
                    if($data['state_guid'] === VSVS_CONST_CREATED) {
                        $this->state_guid = $data['state_guid'] = VSVS_CONST_EDITED;
                    }
                }

             // Assume that every item in the data array has an identically named field in the database and assume every field is meant to be saved
            foreach($data as $label => $values) {
                $fields .= $label . ', ';
                $markedUpFields .= ':' . $label . ', ';
            }

             // The above loop adds an extra ', ' to the end of the strings; remove tbem here
            $fields = substr($fields, 0, -2);
            $markedUpFields = substr($markedUpFields, 0, -2);

            $database_handle = VSVSObject::db();
            $database_statement = $database_handle->prepare('insert into ' . $table . ' (' . $fields . ') values (' . $markedUpFields . ')');
            
            VSVSObject::bindArray($database_statement, $data);

            $database_statement->execute();

            $this->created = date("Y-m-d H:i:s");
        }
       
         /**
          * Function bindArray
          *
          * Combines a prepared statement with it's associated data
          *
          * @param PDOStatement $database_statement The database statement to bind the array to
          * @param Array $data The data to bind
          *
          */
        public static function bindArray(PDOStatement &$database_statement, Array &$data){

            foreach($data as $key => $value) {
                $database_statement->bindValue(':' . $key, $value);
            }
        }

        public function delete() {
            $this->state_guid = VSVS_CONST_DELETED;
        }

         /**
          * Function getByGUID
          *
          * Returns the latest version of an object based on it's GUID or null of no object matches
          * 
          * @param GUID (String) The Hex encoded, upper case GUID of the object to be fetched
          * @param Version GUID (String) The Hex encoded, upper case Version GUID of the object to be fetched
          * @param table (String) The database table to fetch the object from
          */
        public static function getByGUID(String $guid, String $version_guid = '', String $table = 'test_object') {

            $Class = get_called_class(); // Allows us to created instances of this class's children as themselves not VSVSObject

            $database_handle = VSVSObject::db(); // Get the Database Handle
            $database_statement = null;

                if('' !== $version_guid) {

                     // If an exact version is requested return it
                    $database_statement = $database_handle->prepare('select *, hex(guid) as guid, hex(version_guid) as version_guid from ' . $table . ' where guid = :guid and version_guid = :version_guid group by guid');

                    $binary_version_guid = hex2bin($version_guid);
                    $database_statement->bindParam(':version_guid', $binary_version_guid);

                } else {
                     // If no version is requested use the latest version
                     // Use version_guid = in the main query as oposed to in to reduce query cost
                    $database_statement = $database_handle->prepare('select *, hex(guid) as guid, hex(version_guid) as version_guid from ' . $table . ' where version_guid = (
                    Select max(version_guid) from ' . $table . ' where guid = :guid group by guid
                    )');
                }

            $binary_guid = hex2bin($guid);
            $database_statement->bindParam(':guid', $binary_guid);

            $object = null;

            if ($database_statement->execute()) {

                while ($row = $database_statement->fetch(PDO::FETCH_ASSOC)) {
                    $object = new $Class($row, $table);
                }
            }

            return $object;
        }

         /**
          * Function isLatest
          *
          * Returns true if the version_guid supplied is the most recent version of the guid of the object supplied
          * 
          * @param GUID (String) The Hex encoded, upper case GUID of the object to be fetched
          * @param Version GUID (String) The Hex encoded, upper case Version GUID of the object to be checked
          * @param table (String) The database table to fetch the object from
          */
        public static function isLatest(String $guid, String $version_guid, String $table = 'test_object') {

            $Class = get_called_class(); // Allows us to created instances of this class's children as themselves not VSVSObject

            $database_handle = VSVSObject::db(); // Get the Database Handle

             // Use version_guid = in the main query as oposed to in to reduce query cost
            $database_statement = $database_handle->prepare('select guid from ' . $table . ' where version_guid > :version_guid and guid = :guid');

            $binary_guid = hex2bin($guid);
            $database_statement->bindParam(':guid', $binary_guid);

            $binary_version_guid = hex2bin($version_guid);
            $database_statement->bindParam(':version_guid', $binary_version_guid);

            $object = null;

            // If there are any objects with a greater version_guid then this is not the most recent object
            if ($database_statement->execute()) {

                while ($row = $database_statement->fetch(PDO::FETCH_ASSOC)) {
                    return false;
                }
            }
            
            return true;
        }

         /**
          * Function getHistoryByGUID
          *
          * This function returns the entire history of an object
          *
          * @param GUID (String) The Hex encoded, upper case GUID of the object to be fetched
          * @param table (String) The database table to fetch the object from
          * @param sort_order (String) Whether to sort the results in ascending(default) order of creation or descending order - valid values 'asc' and 'desc'
          *
          */
        public static function getHistoryByGUID(String $guid, String $table = 'test_object', String $sort_order = 'asc') {

            $Class = get_called_class();

            $database_handle = VSVSObject::db();

            $database_statement = $database_handle->prepare('select *, hex(guid) as guid, hex(version_guid) as version_guid from ' . $table . ' where guid = :guid order by version_guid ' . $sort_order);

            $binary_guid = hex2bin($guid);
            $database_statement->bindParam(':guid', $binary_guid);
            
            $objects = Array();
            $object = null;

            if ($database_statement->execute()) {

                while ($row = $database_statement->fetch(PDO::FETCH_ASSOC)) {
                    $object = new $Class($row, $table);
                    $objects[] = $object;
                }
            }

            return $objects;
        }

         /**
          * Function getAll
          *
          * This function returns the most recent version of all objects in a given table
          *
          * @param table (String) The database table to fetch the object from
          *
          */
        public static function getAll(String $table = 'test_object') {

            $Class = get_called_class();

            $database_handle = VSVSObject::db();

            $database_statement = $database_handle->prepare('select *, hex(guid) as guid, hex(version_guid) as version_guid from ' . $table . ' where version_guid in (
                Select max(version_guid) from ' . $table . ' group by guid
            ) and state_guid <> :deleted');

            $deleted = VSVS_CONST_DELETED;
            $database_statement->bindParam(':deleted', $deleted);

            $objects = Array();

            if ($database_statement->execute()) {

                while ($row = $database_statement->fetch(PDO::FETCH_ASSOC)) {
                    $objects[] = new $Class($row, $table);
                }
            }

            return $objects;   
        }

         /**
          * Function db
          *
          * This function returns a handle to the current database connection
          *
          */
        public static function db() {

             // If we already have a database handle we will use it and save database connections
            if(!is_null(VSVSObject::$database_handle)) {
                return VSVSObject::$database_handle;
            }

             // Otherwise we must establish a connection
            $database_handle = new PDO(VSVS_DB_HOSTNAME . VSVS_DB_DATABASE, VSVS_DB_USERNAME, VSVS_DB_PASSWORD);
            $database_handle->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_WARNING);

            $offset = 'Europe/London';
            $database_handle->exec("SET time_zone='$offset';");

            VSVSObject::$database_handle = $database_handle;

            return $database_handle;
        }

    } // End Class
    
    /**
     * function shutdown
     *
     * This function sets the database_handle to null so that it will be closed automatically
     *
     */
    function shutdown()
    {
        VSVSObject::$database_handle = null;
    }

    register_shutdown_function('shutdown'); // Register the shutdown function