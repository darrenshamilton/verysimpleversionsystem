<?php

     // Constants 
	define('VSVS_DB_HOSTNAME', 'mysql:host=192.168.0.3:33060');
	define('VSVS_DB_DATABASE', ';dbname=vsvs');
	define('VSVS_DB_USERNAME', 'homestead');
	define('VSVS_DB_PASSWORD', 'secret');

	define('VSVS_CONST_CREATED',  hex2bin('0000000000000000000000000000000000000000'));
	define('VSVS_CONST_EDITED',   hex2bin('0000000000000000000000000000000000000001'));
	define('VSVS_CONST_DELETED',  hex2bin('0000000000000000000000000000000000000002'));
	define('VSVS_CONST_RESTORED', hex2bin('0000000000000000000000000000000000000003'));



     // As this is very experimental
    error_reporting(E_ALL);
    ini_set('display_errors', 1);