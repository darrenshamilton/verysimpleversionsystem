<?php
 /**
  * @Author Darren Hamilton
  * @Date 2017
  * @License MIT
  *
  * Very Simple Version System
  *
  */

     /**
      * Class VSVSObject
      *
      * This is the Base Object which other Versioned Objects should inherit 
      *
      */
    class VSVSState extends VSVSObject {

         /**
          * Function getByGUID
          *
          * Returns the latest version of an object based on it's GUID or null of no object matches
          * 
          * @param GUID (String) The Hex encoded, upper case GUID of the object to be fetched
          * @param Version GUID (String) The Hex encoded, upper case Version GUID of the object to be fetched
          * @param table (String) The database table to fetch the object from
          */
        public static function getByGUID(String $guid, String $version_guid = '', String $table = 'vsvs_state') {
            return parent::getByGUID($guid, $version_guid, $table);
        }

         /**
          * Function isLatest
          *
          * Returns true if the version_guid supplied is the most recent version of the guid of the object supplied
          * 
          * @param GUID (String) The Hex encoded, upper case GUID of the object to be fetched
          * @param Version GUID (String) The Hex encoded, upper case Version GUID of the object to be checked
          * @param table (String) The database table to fetch the object from
          */
        public static function isLatest(String $guid, String $version_guid, String $table = 'vsvs_state') {
            return parent::isLatest($guid, $version_guid, $table);
        }

         /**
          * Function getHistoryByGUID
          *
          * This function returns the entire history of an object
          *
          * @param GUID (String) The Hex encoded, upper case GUID of the object to be fetched
          * @param table (String) The database table to fetch the object from
          * @param sort_order (String) Whether to sort the results in ascending(default) order of creation or descending order - valid values 'asc' and 'desc'
          *
          */
        public static function getHistoryByGUID(String $guid, String $table = 'vsvs_state', String $sort_order = 'asc') {
            return parent::getHistoryByGUID($guid, $table, $sort_order);
        }

         /**
          * Function getAll
          *
          * This function returns the most recent version of all objects in a given table
          *
          * @param table (String) The database table to fetch the object from
          *
          */
        public static function getAll(String $table = 'vsvs_state') {
            return parent::getAll($table);
        }

    } // End Class
