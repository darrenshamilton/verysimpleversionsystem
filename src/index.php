<?php
    // See: /sql/install_test_object_table.sql to create the datable required

	include_once('./VSVSObject.class.php');
	include_once('./VSVSState.class.php');

	?><h1>Usage Examples</h1>

    <pre><?php
        // The default object saves to the test_object table and has a name property
        // Once saved it will be given a guid and a version_id locally
        // To verify it has been saved, fetch it from the database and verify it has a created field

        $local_object = new VSVSObject(array('name' => 'Test'));

        // Verify we can create an object, save it, modify it and save it again
        $local_object->save();

        $original_version = $local_object->version_guid;

        $local_object->name = 'New Name';
        $local_object->save();

        $local_object->name = 'Newest Name';
        $local_object->save();

        // Fetch the latest version of an object from the database using it's GUID
        $database_object = VSVSObject::getByGUID($local_object->guid); // fetch the database's copy of the object - it should also have a created field

        VSVSObject::isLatest($local_object->guid, $original_version); // is our original version still the latest? Should return false
        VSVSObject::isLatest($local_object->guid, $database_object->version_guid); // Is the version we fetched from the database the latest version? Should return true
        VSVSObject::isLatest($local_object->guid, $local_object->version_guid); // Is the latest version attached to our local copy of the object the latest version? Should return true

        $database_object = VSVSObject::getByGUID($local_object->guid, $original_version); // fetch the database's copy of the object - it should also have a created field

        // We can also get all the most recent versions of all the objects
        $all = VSVSObject::getAll();
    
        $local_object->name = 'Deleted';
        $local_object->save();

        $local_object->delete();
        $local_object->save();


        $all = VSVSObject::getAll();

        // Or the history of a specific object
        $history = VSVSObject::getHistoryByGUID($local_object->guid);

    ?></pre>