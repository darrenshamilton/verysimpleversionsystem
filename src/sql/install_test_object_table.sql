# It will be useful to disable foreign key checks during the uninstall process later
# Might as well add it now

	SET FOREIGN_KEY_CHECKS=0;

		DROP TABLE IF EXISTS `test_object`;
		DROP TABLE IF EXISTS `vsvs_state`;

		CREATE TABLE IF NOT EXISTS `test_object` (
			`guid` binary(20),
			`version_guid` binary(20),
			`name` VARCHAR(45) NOT NULL,
			`created` DATETIME DEFAULT CURRENT_TIMESTAMP,
			`state_guid` binary(20),
			PRIMARY KEY (`guid`, `version_guid`),
			INDEX `state_guid` (`state_guid` ASC),
			INDEX `guid` (`guid` ASC),
			INDEX `version_guid` (`version_guid` ASC),
            CONSTRAINT `state_guid_fk` FOREIGN KEY (`state_guid`) REFERENCES `vsvs_state`(`guid`)
        )
        ENGINE = InnoDB;

		CREATE TABLE IF NOT EXISTS `vsvs_state` (
			`guid` binary(20),
			`version_guid` binary(20),
			`name` VARCHAR(45) NOT NULL,
			`created` DATETIME DEFAULT CURRENT_TIMESTAMP,
			PRIMARY KEY (`guid`, `version_guid`),
			INDEX `guid` (`guid` ASC),
			INDEX `version_guid` (`version_guid` ASC)
        )
        ENGINE = InnoDB;

        INSERT INTO `vsvs_state` (`name`, `guid`, `version_guid`) values ("CREATED", unhex('0000000000000000000000000000000000000000'), unhex('0000000000000000000000000000000000000000'));
        INSERT INTO `vsvs_state` (`name`, `guid`, `version_guid`) values ("EDITED", unhex('0000000000000000000000000000000000000001'), unhex('0000000000000000000000000000000000000001'));
        INSERT INTO `vsvs_state` (`name`, `guid`, `version_guid`) values ("DELETED", unhex('0000000000000000000000000000000000000002'), unhex('0000000000000000000000000000000000000002'));
        INSERT INTO `vsvs_state` (`name`, `guid`, `version_guid`) values ("RESTORED", unhex('0000000000000000000000000000000000000003'), unhex('0000000000000000000000000000000000000003'));

	SET FOREIGN_KEY_CHECKS=1;